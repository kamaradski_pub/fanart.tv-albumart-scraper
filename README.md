# Documentation #

Home: https://gitlab.kamaradski.com/kamaradski/Fanart.tv_AlbumArt_scraper

This script is a stand alone python scraper that will read MusicBrainz tags of your MP3 and FLAC files, and fetch the following files from the Fanart.tv database:  
* Cover artwork  
* CDart Artwork  

***
## Dependencies: ##

* Python
* Linux (might work under Windows too, but totally untested)
* Mutagen libs (https://bitbucket.org/lazka/mutagen)
* Your music collection is tagged with at least "MusicBrainz Album ID"
* Each album has it's own folder

***
## Instructions: ##

* Make sure to meet the dependencies above
* Toss the .py script in your audio folder root directory (or any folder that has sub-folders with music)
* Optional: set the VerboseOutput and LogDownloadArtwork flags to your liking inside the .py script
* Run it with "python Fanart_AlbumArt_scraper.py"
* Sit back and relax, this will take a while depending on the size of your collection
* After it completed it's run, check the log-files created in the same directory where you have run the script
* Your newly downloaded artwork will automatically be visible in Kodi (must be supported by your skin)

***
## Current features & Limitations: ##

**Project state: LIVE**  

* Limitations:  
    * Support for Mp3 & FLAC only  
	* Support for cover.jpg & cdart.png only  
	* Relies only on MusicBrainz tags  
	* Downloads only from Fanart.tv
  * Speed of processing is only limited by your machine (+1 second per API-call to avoid rate-limiting)
  * Script runs fully in memory, with extremely large collections this might consume ~80MB


* Features:  
	* Logging:  
		* Debug verbose logging of every action to file (in real-time)
		* Log downloaded artwork to file (in real-time)  
	* Download:  
		* Download cover-art to cover.jpg  
		* Download CD-art to cdart.png (Will try to match specific disk numbers, or fall back to first result)  
		* Reduced API calls by skipping albums that already contain a full set of artwork  
		* Support for Fanart.tv user API keys to get more up-to-date results
		* Supports MusicBrainz tags, and MusicBrainz API  
		* Supports Fanart.tv API  
		* Supports FLAC & MP3  
  * Extensive error handling
  * Execution fully in memory

***
## How does it work on the inside ? ##

1. Create list of sub-folders that contain music files (list-1)
2. Loop through list-1 and create a list of folders that require cover-art
3. Loop through list-1 and create a list of folders that require cd-art
4. Delete list-1 & merge the 2 new lists into 1 master-list that needs action
5. For each folder:
  * Loop through each FLAC/MP3 and read all Musicbrainz AlbumID's and Disknumber
  * Decide which AlbumID and Disknumber are most common for this folder
  * Contact Musicbrainz API and find the ReleaseID for this AlbumID
  * Contact the Fanart.TV API and fetch the JSON for this ReleaseID
  * Decide what we need to download and download it
  * loop-on!

***
## Disclaimer ##

USE AT YOUR OWN RISK!!

This script contains a number of file-manipulation features, it is always risky to run externally obtained code on your local machine. In addition i code for fun in the little free-time i have available, so don't expect any professional coding skills being implemented here. Though i have extensively tested this on my own machine, I recommend you make a backup of your files before running this script.

Use, edit, change, re-use, distribute, or use commercially as you wish, as long as you keep my credits intact and follow the TOCs of the respective MusicBrainz.org & Fanart.tv APIs. I would appreciate if you make changes\optimisations\tweaks, to keep me informed so I can add your work to any new versions I might or might not release in the future.  

**Last but not least, if you like my work, and appreciate my efforts: leave me a small donation via my website https://kamaradski.com**  

***

## Change-log

**V2.0**
- Rewrite the whole thing from scratch, fully new script logic
- Finally truly stable due to extensive new error-handling
- Large processing speed win
- Better clear logging
- This update was long overdue and fixes countless of issues and bugs (and other bad coding skills)

**V1.6**  
- Improved API error handling for Fanart.tv API calls
- Minor error-logging tweaks  
- Minor display messages tweaks  
- Fixed possible divide-by-zero exceptions upon statistic calculation   
- Added Toggle Switch for Writing downloaded artwork log  
- Splitted downloaded & missing artwork in 2 separate log-files  
- New file-names for the log-files  
- Optimized initial file initiating process  

**V1.5.3**  
- Improved API error handling for MusicBrainz API calls  
- Added API-errors to the debug-log  

**V1.5.2**  
- Small code-flow correction to improve file-type detection  
- Improve socket error handling  
- Library import clean-up  
- Added toggle for writing missing artwork to file  
- Changed project status to: STABLE   

**V1.5.1**
- Code clean-up release, many new comments added  

**V1.5**  
- Fixed a bug where FLAC files without a disk-number would cause an exception  
- Fixed a logging exception when Fanart.tv API is not available  
- Fixed a logical error in the audio-type detection  
- Fixed a bug with FLAC AlbumID detection  
- Added debug-logging switch  
- Added pre-defined function for debug-log writing, to allow code clean-up  
- Generic code-clean-up  

**V1.4**  
- Added Summery log-file keeping track of missing & downloaded Fanart  
- Missing Fanart no longer include Fanart downloaded in the current session  
- Added FLAC support  
- Minor changes to the on-screen & log-file  messages  

**V1.3**  
- Additional statistics added  
- Improved error handling  
- Added support for Client-API key  
- Added & tweaked more UI messages  
- Added some configuration options to the script (not yet working)  
- Added more Fanart.tv messages to the script header, to comply to their API rules  

**V1.2**  
- Added User-Agent headers to the API-call request, for easy identification\logging\reporting on their end  
- Minor ui message tweaks  
- Added basic statistic reporting (WIP)  

**V1.1**  
- Greatly improved user on-screen messages  
- Greatly improved logging  
- Generic code clean-up  
- Improved error handling for the API-calls  
- File header with credits  

**V1.0**  
- Stability tweaks  

**V0.0**  
- Initial release
