# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# File:			Fanart_AlbumArt_scraper.py
# Version:		V2.0
# Author:		Kamaradski 2015
# Contributors:	None
#
# Standalone Python cover and cdart scraper for Fanart.tv
#
# This script heavily relies on the Fanart.tv database
# Please contribute to this database, by submitting your own artwork to the site
#
# https://gitlab.kamaradski.com/kamaradski/Fanart.tv_AlbumArt_scraper
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# Your personal Fanart.tv API key for more recent results (https://fanart.tv/get-an-api-key/)
_UserAPIkey = "your API key here"

# Set to 'True' if you like to read, or 'False' if you only care about results
_VerboseOutput = True

# Set to True to ENABLE downloaded artwork log-file
LogDownloadArtwork = True

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# 					DO NOT EDIT BELOW HERE
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# -=-=-=-=-=-=-=-=-=- Loading libraries -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
import os
from os.path import isfile, join
import time
from datetime import datetime
from mutagen.id3 import ID3
from mutagen.flac import FLAC
import itertools
import operator
import urllib
import urllib2
import socket
from xml.dom.minidom import parse
import json

# -=-=-=-=-=-=-=-=-=- Pre-set Variables -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_timeStartTime = time.strftime("%Y-%m-%d %H:%M:%S")
_rootDir = os.path.dirname(os.path.realpath(__file__))
_strDownloadLogFilePath = (os.path.dirname(os.path.realpath(__file__)) + '/Fanart_downloadlog.log')
_listFoldersWithMedia = []
_listCoverArtRequired = []
_listCDArtRequired = []

# -=-=-=-=-=-=-=-=-=- Define Functions -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# ---------------------   Delete (&re-create) Fanart_downloadlog.log if exist:
def setupdownloadlog():
	if os.path.isfile(_strDownloadLogFilePath):
		os.remove(_strDownloadLogFilePath)
	else:
		f= open(_strDownloadLogFilePath,"w+")
		if _VerboseOutput: print 'Download log-file created'
		f.close()


# ---------------------   Write to Fanart_downloadlog.log:
def writedownloadlog(_WriteMe):
	_handle = open(_strDownloadLogFilePath, "a+")
	_handle.write(_WriteMe)
	_handle.close()


# ---------------------   Test Fanart.tv _UserAPIkey for valid length
def testFanartAPIkey(_UserAPIkey):
	if len(_UserAPIkey) > 30:
		if _VerboseOutput:
			_strDebugMessage = ('Setting: Using Fanart.tv Client API Key: %s' % _UserAPIkey)
			print _strDebugMessage

	else:
		if _VerboseOutput:
			_strDebugMessage = ('Setting: Fanart.tv Client API key NOT set or invalid: %s' % _UserAPIkey)
			print _strDebugMessage
		_UserAPIkey = False
	return _UserAPIkey


# ---------------------   Return most common element in a list

def most_common(L):
	# https://stackoverflow.com/a/1520716/3923136 --> Alex Martelli 2009
	# get an iterable of (item, iterable) pairs
	SL = sorted((x, i) for i, x in enumerate(L))
	# print 'SL:', SL
	groups = itertools.groupby(SL, key=operator.itemgetter(0))
	# auxiliary function to get "quality" for an item
	def _auxfun(g):
		item, iterable = g
		count = 0
		min_index = len(L)
		for _, where in iterable:
		  count += 1
		  min_index = min(min_index, where)
		# print 'item %r, count %r, minind %r' % (item, count, min_index)
		return count, -min_index
	# pick the highest-count/earliest item
	return max(groups, key=_auxfun)[0]


# ---------------------   Create DB of available Media locations
def MediaScanning():
	_TotalMediaCounter = 0
	_folderCounter = 0
	if _VerboseOutput: print ''

	for dirName, subdirList, fileList in os.walk(_rootDir):
		_strCurrentFolder = dirName
		_MediaCounter = 0

		for _currFile in fileList:
			_isMedia = _currFile.lower().endswith(('.mp3', '.flac'))
			if _isMedia: _MediaCounter = _MediaCounter + 1

		if _MediaCounter > 0:
			_listFoldersWithMedia.append(_strCurrentFolder)
			_folderCounter = _folderCounter + 1
			_TotalMediaCounter = _TotalMediaCounter + _MediaCounter
			if _VerboseOutput:
				_strDebugMessage = ('Media found: %s' % _strCurrentFolder)
				print _strDebugMessage

	if _VerboseOutput: print ''
	_strDebugMessage = ('Total folders that contain media: %s' % _folderCounter)
	print _strDebugMessage
	_strDebugMessage = ('Total media-files found: %s' % _TotalMediaCounter)
	print _strDebugMessage
	print ''
	return _listFoldersWithMedia


# ---------------------   Create DB of missing cover-art
def CoverArtScan(_listFoldersWithMedia):
	if _VerboseOutput: print ''

	for _strCurrentFolder in _listFoldersWithMedia:
		_FilesInFolder = [f for f in os.listdir(_strCurrentFolder) if isfile(join(_strCurrentFolder, f))]
		if 'cover.jpg' in _FilesInFolder:
			if _VerboseOutput:
				_strDebugMessage = ('Cover-art found in: %s' % _strCurrentFolder)
				print _strDebugMessage
		else:
			_listCoverArtRequired.append(_strCurrentFolder)

	if _VerboseOutput: print ''
	_Counter = len(_listCoverArtRequired)
	_strDebugMessage = ('Total Coverart missing: %s' % _Counter)
	print _strDebugMessage

	return _listCoverArtRequired


# ---------------------   Create DB of missing cd-art
def CDArtScan(_listFoldersWithMedia):
	if _VerboseOutput: print ''

	for _strCurrentFolder in _listFoldersWithMedia:
		_FilesInFolder = [f for f in os.listdir(_strCurrentFolder) if isfile(join(_strCurrentFolder, f))]
		if 'cdart.png' in _FilesInFolder:
			if _VerboseOutput:
				_strDebugMessage = ('CD-art found in: %s' % _strCurrentFolder)
				print _strDebugMessage
		else:
			_listCDArtRequired.append(_strCurrentFolder)

	if _VerboseOutput: print ''
	_Counter = len(_listCDArtRequired)
	_strDebugMessage = ('Total Coverart missing: %s' % _Counter)
	print _strDebugMessage

	return _listCDArtRequired


# ---------------------   Find Flac AlbumID
def dealWithFLAC(_FoundFLAC):
	_listMBalbumIDFound =[]
	_listMBTPOSFound = []
	for i in _FoundFLAC:
		if i.lower().endswith(('.flac')):
			if _VerboseOutput: print ('Processing "%s"' % os.path.basename(i))
			_FileProcessing = FLAC(i)

			try:
				_MBalbumID = _FileProcessing.get('musicbrainz_albumid')[0]
				_listMBalbumIDFound.append(_MBalbumID)
				_MBTPOS = _FileProcessing.get('discnumber')[0]
				_MBcurrDisk = _MBTPOS[0][0]
				_listMBTPOSFound.append(_MBcurrDisk)
			except TypeError:
				if _VerboseOutput: print ('No valid Tags detected for: %s' % os.path.basename(i))

	if len(_listMBalbumIDFound) < 1:
		if _VerboseOutput: print ('No valid Tags detected')
		_MBalbumIDFound = None
		_MBTPOSFound = None
	else:
		_MBalbumIDFound = most_common(_listMBalbumIDFound)
		_MBTPOSFound = most_common(_listMBTPOSFound)

	return [_MBalbumIDFound, _MBTPOSFound]


# ---------------------   Find MP3 AlbumID
def dealWithMP3(_FoundMP3):
	_listMBalbumIDFound =[]
	_listMBTPOSFound = []
	for i in _FoundMP3:
		if i.lower().endswith(('.mp3')):
			if _VerboseOutput: print ('Processing "%s"' % os.path.basename(i))
			#_FileProcessing = ID3(i)
			try:
				curTAGinfo = ID3 (i)
				_MBalbumID = curTAGinfo.get('TXXX:MusicBrainz Album Id')
				_listMBalbumIDFound.append(_MBalbumID)
				_MBTPOS = curTAGinfo.get('TPOS')
				_listMBTPOSFound.append(_MBTPOS)
			except TypeError:
				if _VerboseOutput: print ('No valid Tags detected for: %s' % os.path.basename(i))

	if len(_listMBalbumIDFound) < 1:
		if _VerboseOutput: print ('No valid Tags detected')
		_MBalbumIDFound = None
		_MBTPOSFound = None
	else:
		_MBalbumIDFound = most_common(_listMBalbumIDFound)
		_MBTPOSFound = most_common(_listMBTPOSFound)

	return [_MBalbumIDFound, _MBTPOSFound]


# ---------------------   Query MusicBrainz for ReleaseGroup
def queryMBGroupID(_strMBAlbumIDFound):
	_strMBAPIreq = 'http://musicbrainz.org/ws/2/release/' + str(_strMBAlbumIDFound) + '?inc=release-groups'
	if _VerboseOutput: print ('URL= %s' % _strMBAPIreq)
	FetchedXML = None
	skipit = False
	max_attempts = 3
	r=0

	# ---------------------   MusicBrainz API Request
	while FetchedXML is None and r<max_attempts:
		try:
			if r==(max_attempts-1):
				if _VerboseOutput: print 'No MusicBrainz result....Skipping'
				skipit = True
				break
			req2 = urllib2.Request(_strMBAPIreq, headers = { "User-Agent" : "Kamaradski_scraper/V2.0" })
			FetchedXML = parse(urllib2.urlopen( req2, timeout=10))

		# ---------------------   Error-handling for http error-codes
		except urllib2.HTTPError as e2:
			_DetectedError = e2.code
			if _VerboseOutput: print "MusicBrainz API call returned HTTP error: ",_DetectedError
			r=r+1
			print "Re-trying MusicBrainz API-call, attempt -- ",r
			time.sleep(10)
			pass

		# ---------------------   Error-handling for URL errors
		except urllib2.URLError as e2:
			_DetectedError = e2.args
			if _VerboseOutput: print "MusicBrainz API call returned URL error: ",_DetectedError
			r=r+1
			print "Re-trying MusicBrainz API-call, attempt -- ",r
			time.sleep(10)
			pass

		# ---------------------   Error-handling for socket errors
		except socket.timeout, e2:
			if _VerboseOutput: print "MusicBrainz API call returned socket error: ",e2
			r=r+1
			print "Re-trying MusicBrainz API-call, attempt -- ",r
			time.sleep(10)
			pass

	# ---------------------   Process MusicBrainz API result
	if not skipit:
		relGroupElem = FetchedXML.getElementsByTagName('release-group')[0]
		_MBrelGroupID = relGroupElem.getAttribute('id')
	return _MBrelGroupID


# ---------------------   Create FanArt Query URL
def _createqueryFanartTV(_MBrelGroupID, _UserAPIkey, _intDiskID):
	if _UserAPIkey:
		_fanartTVreq = 'http://webservice.fanart.tv/v3/music/albums/' + _MBrelGroupID + '?api_key=6f1f3b034c7b179b824d9641425cbd24' + '&client_key=' + _UserAPIkey
	else:
		_fanartTVreq = 'http://webservice.fanart.tv/v3/music/albums/' + _MBrelGroupID + '?api_key=6f1f3b034c7b179b824d9641425cbd24'
	if _VerboseOutput: print ('URL = %s' % _fanartTVreq)
	_fanartQueryURL = urllib2.Request(_fanartTVreq, headers = { "User-Agent" : "Kamaradski_scraper/V2.0" })
	return _fanartQueryURL


# --------------------- Query Fanart.tv and related error handling
def queryFanartTV(_fanartQueryURL):
	_FanartAPIReply = None
	_e = None
	_DetectedError = None
	max_attempts = 3
	r=0

	# ---------------------   Fanart.tv API request
	while _FanartAPIReply is None and r<max_attempts:
		try:
			if r==(max_attempts-1):
				if _VerboseOutput: print 'Fanart.tv API error.... Skipping'
				skipit = True
				break
			_FanartAPIReply = urllib2.urlopen(_fanartQueryURL, timeout=10).read()

		# ---------------------   Error-handling
		except urllib2.HTTPError as _e:
			_DetectedError = _e.code
			if _e.code == 404:
				if _VerboseOutput: print "No results in Fanart.tv database for this AlbumID. Error-code: ",_DetectedError
				break
			elif _VerboseOutput: print "Fanart.tv API call returned HTTP error: ",_DetectedError
			r=r+1
			print "Re-trying Fanart API-call, attempt -- ",r
			time.sleep(15)
			pass

		# ---------------------   Error-handling for URL errors
		except urllib2.URLError as _e:
			_DetectedError = _e.args
			if _VerboseOutput: print "Fanart.tv API call returned URL error: ",_DetectedError
			r=r+1
			print "Re-trying Fanart.tv API-call, attempt -- ",r
			time.sleep(10)
			pass

		# ---------------------   Error-handling for socket errors
		except socket.timeout, _e:
			if _VerboseOutput: print "Fanart.tv API call returned socket error: ",_e
			r=r+1
			print "Re-trying Fanart.tv API-call, attempt -- ",r
			time.sleep(10)
			pass
	return _FanartAPIReply


# ---------------------   Process Fanart.tv result
def ProcessFanartResult(_FanartAPIReply, _MBrelGroupID, _FilesInFolder, _strCurrentFolder, _intDiskID):
	_fanartTVdata = json.loads(_FanartAPIReply)
	artlist = []
	_listDownloadedArtwork = []
	_intNewCoversDownloaded = 0
	_intNewDiskArtDownloaded = 0

	# ---------------------   Download Cover-art
	for i in _fanartTVdata["albums"][_MBrelGroupID]:
		artlist.append(i)
	if 'cover.jpg' in _FilesInFolder:
		if _VerboseOutput: print ('Local cover-art detected, skipping download')
	elif 'albumcover' in artlist:
			_fanartCoverURL = _fanartTVdata["albums"][_MBrelGroupID]["albumcover"][0]["url"] # the [0] here assures to download the artwork with the highest 'likes' score.
			dload = urllib.URLopener()
			dload.retrieve(_fanartCoverURL, (_strCurrentFolder + "/cover.jpg"))
			_listDownloadedArtwork.append(_fanartCoverURL)
			_intNewCoversDownloaded = _intNewCoversDownloaded + 1
	elif 'albumcover' not in artlist:
		if _VerboseOutput: print ('Cover-art not available on Fanart.tv')

	if 'cdart.png' in _FilesInFolder:
		if _VerboseOutput: print ('Local CD-art detected, skipping download')
	elif 'cdart' in artlist:
		# ---------------------   Pre-process available URL's
		_listFanartCDArtURL = []
		comparecheck = []
		if _intDiskID >0:
			_fanartCDArtURLiter = _fanartTVdata["albums"][_MBrelGroupID]["cdart"]
			for a in _fanartCDArtURLiter:
				_intCompareDisk = a["disc"]
				if _intCompareDisk == _intDiskID:
					_listFanartCDArtURL.append(a["url"])
					comparecheck.append(a["disc"])
			# ---------------------   Download cdart
			if _intDiskID in comparecheck:
				if _VerboseOutput: print ('Exact disk found: %s' % _listFanartCDArtURL[0])
				dload = urllib.URLopener()
				dload.retrieve(_listFanartCDArtURL[0], (_strCurrentFolder + "/cdart.png"))
				_listDownloadedArtwork.append(_listFanartCDArtURL[0])
				_intNewDiskArtDownloaded = _intNewDiskArtDownloaded +1
			else:
				_strDLThisInstead = _fanartTVdata["albums"][_MBrelGroupID]["cdart"][0]["url"]
				dload = urllib.URLopener()
				if _VerboseOutput: print ('Generic disk found: %s' % _strDLThisInstead)
				dload.retrieve(_strDLThisInstead, (_strCurrentFolder + "/cdart.png"))
				_listDownloadedArtwork.append(_strDLThisInstead)
				_intNewDiskArtDownloaded = _intNewDiskArtDownloaded +1
		else:
			_strDLThisInstead = _fanartTVdata["albums"][_MBrelGroupID]["cdart"][0]["url"]
			dload = urllib.URLopener()
			if _VerboseOutput: print ('Generic disk found: %s' % _strDLThisInstead)
			dload.retrieve(_strDLThisInstead, (_strCurrentFolder + "/cdart.png"))
			_listDownloadedArtwork.append(_strDLThisInstead)
			_intNewDiskArtDownloaded = _intNewDiskArtDownloaded +1

	elif 'cdart' not in artlist:
		if _VerboseOutput: print ('CD-art not available on Fanart.tv')

	return (_listDownloadedArtwork, _intNewCoversDownloaded, _intNewDiskArtDownloaded)


# ---------------------   Select FLAC or MP3
def MBAlbumIDSelect(_listCoverArtRequired, _listCDArtRequired, _UserAPIkey):
	_ItemsToAction = list(set(_listCoverArtRequired + _listCDArtRequired))
	_listCoverArtRequired = [] #cleanup
	_listCDArtRequired = [] #cleanup

	_intTotalNewCoversDownloaded = 0
	_intTotalNewDiskArtDownloaded = 0
	_counter = 0
	_initial = len(_ItemsToAction)

	for _strCurrentFolder in _ItemsToAction:
		_counter = _counter + 1
		_FetchingProcess = 100.00*float(_counter)/float(_initial)
		print 'processing folder: {} of {} = {}%'.format(_counter, _initial, _FetchingProcess)
		_FilesInFolder = [f for f in os.listdir(_strCurrentFolder) if isfile(join(_strCurrentFolder, f))]
		_FoundMP3 = []
		_FoundFLAC = []
		_listFileTypeFound = []

		for _currFile in _FilesInFolder:
			_isMP3 = _currFile.lower().endswith(('.mp3'))
			if _isMP3:
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_FoundMP3.append(_ToAdd)
				_listFileTypeFound.append ('MP3')
			_isFLAC = _currFile.lower().endswith(('.flac'))
			if _isFLAC:
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_listFileTypeFound.append ('FLAC')
				_FoundFLAC.append(_ToAdd)

		_MostCommonFileType = most_common(_listFileTypeFound)
		print ('For: %s' % _strCurrentFolder)
		if _MostCommonFileType == 'FLAC':
			_TagsFound = dealWithFLAC(_FoundFLAC)
		elif _MostCommonFileType == 'MP3':
			_TagsFound = dealWithMP3(_FoundMP3)
		print 'Found ReleaseID: {} and CD# {}'.format(_TagsFound[0], _TagsFound[1])

		if _TagsFound[0] is not None:
			_MBrelGroupID = queryMBGroupID(_TagsFound[0])
			if _VerboseOutput: print ('MBGroupID= %s' % _MBrelGroupID)

			_fanartQueryURL = _createqueryFanartTV(_MBrelGroupID, _UserAPIkey, _TagsFound[1])

			_FanartAPIReply = queryFanartTV(_fanartQueryURL)
			if _VerboseOutput: print ('Fanart result= %s' % str(_FanartAPIReply))

			if _FanartAPIReply:
				_listDownloadedArtwork, _intNewCoversDownloaded, _intNewDiskArtDownloaded = ProcessFanartResult(_FanartAPIReply, _MBrelGroupID, _FilesInFolder, _strCurrentFolder, _TagsFound[1])
				_intTotalNewCoversDownloaded = _intTotalNewCoversDownloaded + _intNewCoversDownloaded
				_intTotalNewDiskArtDownloaded = _intTotalNewDiskArtDownloaded + _intNewDiskArtDownloaded
				if (len(_listDownloadedArtwork)) > 0:
					print ('Downloaded:')
					print '\n'.join(_listDownloadedArtwork)
					if LogDownloadArtwork: writedownloadlog('For: %s \n' % _strCurrentFolder)
					if LogDownloadArtwork: writedownloadlog('\n'.join(_listDownloadedArtwork))
					if LogDownloadArtwork: writedownloadlog('\n\n')
			else:
				print ('No downloadable artwork found')
		else:
			print ('No valid MusicBrainz tags detected')
		print ''
		print ''
		time.sleep(1) #try to avoid overloading the MB & Fanart API's

	return (_intTotalNewCoversDownloaded, _intTotalNewDiskArtDownloaded)


# -=-=-=-=-=-=-=-=-=- Start main loop -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
print ''
print ''
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print '           Python Fanart.tv Album Artwork Scraper by Kamaradski V2.0'
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print ''
print ''
_UserAPIkey = testFanartAPIkey(_UserAPIkey)
if LogDownloadArtwork: setupdownloadlog()
print ''
print ''
_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Finding Media Locations **********' % _timeNOW)
_listFoldersWithMedia = MediaScanning()
print ''
print ''
_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Finding Cover art **********' % _timeNOW)
_listCoverArtRequired = CoverArtScan(_listFoldersWithMedia)
print ''
print ''
_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Finding CD art **********' % _timeNOW)
_listCDArtRequired = CDArtScan(_listFoldersWithMedia)
_mediaFolder = len(_listFoldersWithMedia) #take stats be4 deleting the content of this list
_listFoldersWithMedia = [] #Free-up some memory
print ''
print ''
_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Finding missing artwork **********' % _timeNOW)
print ''
print ''
_coversRequired = len(_listCoverArtRequired)#take stats be4 manipulating the content of this list
_cdartRequired = len(_listCDArtRequired)
_intTotalNewCoversDownloaded, _intTotalNewDiskArtDownloaded = MBAlbumIDSelect(_listCoverArtRequired, _listCDArtRequired, _UserAPIkey)
print ''
print ''
_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
_timeSTOP = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: statistics **********' % _timeNOW)
print ''
_existingCoverArt = (_mediaFolder-_coversRequired)
_existingDiskArt = (_mediaFolder-_cdartRequired)
_coverCoverage = 100.00*float(_coversRequired)/float(_mediaFolder)
_cdartCoverage = 100.00*float(_cdartRequired)/float(_mediaFolder)

print 'Before the scan:'
print 'Found {} folders that contain media)'.format(_mediaFolder)
print ''
print 'Missing {} cover-art = {}%'.format(_coversRequired, _coverCoverage)
print 'Missing {} disk-art = {}%'.format(_cdartRequired, _cdartCoverage)
print ''
print 'New cover-art downloaded = {}'.format(_intTotalNewCoversDownloaded)
print 'New disk-art downloaded = {}'.format(_intTotalNewDiskArtDownloaded)
print ''
print 'After the scan:'
_coversRequired = _coversRequired-_intTotalNewCoversDownloaded
_cdartRequired = _cdartRequired-_intTotalNewDiskArtDownloaded
_coverCoverage = 100.00*float(_coversRequired)/float(_mediaFolder)
_cdartCoverage = 100.00*float(_cdartRequired)/float(_mediaFolder)
print 'Missing {} cover-art = {}%'.format(_coversRequired, _coverCoverage)
print 'Missing {} disk-art = {}%'.format(_cdartRequired, _cdartCoverage)
print ''
print ''
print 'Scan started: {}'.format(_timeStartTime)
print 'Scan completed: {}'.format(_timeSTOP)
d1 = datetime.strptime(_timeStartTime, "%Y-%m-%d %H:%M:%S")
d2 = datetime.strptime(_timeSTOP, "%Y-%m-%d %H:%M:%S")
_timeDelta = abs((d2 - d1).seconds)
_timeDelta = time.strftime('%H:%M:%S', time.gmtime(_timeDelta))
print 'Scanning duration: {}'.format(_timeDelta)
print ''
print ''
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print '           Python Fanart.tv Album Artwork Scraper by Kamaradski V2.0'
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print ''
print ''

# EOF
