# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# File:			Fanart_maintenance.py
# Version:		V1.0
# Author:		Kamaradski 2017
# Contributors:	None
#
# Standalone Python cover and cdart maintenance script
#
# it will delete:
# cover.jpg & cdart.png < 1000x1000px
#	(larger resulutions will be marked for manual review)
# folder.jpg
# Thumb.db
# "~/#recycle" paths
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

#notes:
#add: .DS_Store
# Folder.jpg Capital F

# Set to 'True' if you like to read, or 'False' if you only care about results
_VerboseOutput = True

# Set to True to ENABLE deleted file log-file
LogDeletedFiles = True

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# 					DO NOT EDIT BELOW HERE
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# -=-=-=-=-=-=-=-=-=- Loading libraries -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
import os
from os.path import isfile, join
import time
from datetime import datetime
from PIL import Image #need 'pip install pillow'
import shutil


# -=-=-=-=-=-=-=-=-=- Pre-set Variables -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

_timeStartTime = time.strftime("%Y-%m-%d %H:%M:%S")
_rootDir = os.path.dirname(os.path.realpath(__file__))
_strDeletedFileLogPath = (os.path.dirname(os.path.realpath(__file__)) + '/Fanart_deletelog.log')


# -=-=-=-=-=-=-=-=-=- Define Functions -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


# ---------------------   Delete (&re-create) Fanart_downloadlog.log if exist:
def setupDeletedFileLog():
	if os.path.isfile(_strDeletedFileLogPath):
		os.remove(_strDeletedFileLogPath)
	else:
		f= open(_strDeletedFileLogPath,"w+")
		if _VerboseOutput: print 'Deleted file log created'
		f.close()


# ---------------------   Write to Fanart_downloadlog.log:
def writeDeletedLog(_WriteMe):
	_handle = open(_strDeletedFileLogPath, "a+")
	_handle.write(_WriteMe)
	_handle.close()


# ---------------------   Scanning folders & Sub-Folders
def FolderScanning():
	_GarbageFound = []
	_CoverFilesFound = []
	_CDArtFilesFound = []
	_FolderJPGFound = []
	_ThumbsDBFound = []
	_FolderCount = 0

	if _VerboseOutput: print ''
	for dirName, subdirList, fileList in os.walk(_rootDir):
		_strCurrentFolder = dirName
		if _VerboseOutput: print 'Scanning {}'.format(_strCurrentFolder)
		_FolderCount = _FolderCount + 1

		if '#recycle' in _strCurrentFolder:
			_GarbageFound.append(_strCurrentFolder)
			print 'NOT OK: {}'.format(_strCurrentFolder)
		#else:
		for _currFile in fileList:
			if _currFile == 'cover.jpg':
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_CoverFilesFound.append(_ToAdd)
				#if _VerboseOutput: print '--> {}'.format(_currFile)
			if _currFile == 'cdart.png':
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_CDArtFilesFound.append(_ToAdd)
				#if _VerboseOutput: print '--> {}'.format(_currFile)
			if _currFile == 'folder.jpg':
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_FolderJPGFound.append(_ToAdd)
				#if _VerboseOutput: print '--> {}'.format(_currFile)
			if _currFile == 'Thumbs.db':
				_ToAdd = _strCurrentFolder+'/'+_currFile
				_ThumbsDBFound.append(_ToAdd)
				#if _VerboseOutput: print '--> {}'.format(_currFile)
	return (_CoverFilesFound, _CDArtFilesFound, _FolderJPGFound, _ThumbsDBFound, _FolderCount, _GarbageFound)

# ---------------------   Delete a list of files
def DeleteListOfFiles(_listDeleteThis):
	_DeletedFiles = []
	if _VerboseOutput: print ''
	for i in _listDeleteThis:
		try:
			os.remove(i)
			_Mssg = 'Deleted: {}\n'.format(i)
			if _VerboseOutput: print _Mssg
			if LogDeletedFiles: writeDeletedLog(_Mssg)
			_DeletedFiles.append(i)
		except:
			pass #just skipping here for now
	return _DeletedFiles

# ---------------------   Detect image resolution
def ImageResolution(_file):
	if _file.endswith(('.jpg', '.png')):
		try:
			img = Image.open(os.path.join(os.curdir, _file))
			_size = img.size
		except IOError:
			_size = -1
			print 'ERROR: file cannot be read: {}'.format(_file)
	return _size

# -=-=-=-=-=-=-=-=-=- Start main loop -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
print ''
print ''
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print '           Fanart maintenance script by Kamaradski V1.0'
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print ''
print ''
if LogDeletedFiles: setupDeletedFileLog()

_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Scanning folders & sub-folders **********' % _timeNOW)
_CoverFilesFound, _CDArtFilesFound, _FolderJPGFound, _ThumbsDBFound, _FolderCount, _GarbageFound = FolderScanning()
_intCoverfound = len(_CoverFilesFound)
_intCDArtfound = len(_CDArtFilesFound)
_intFolderJPGfound = len(_FolderJPGFound)
_intThumbsDBfound = len(_ThumbsDBFound)
_intGarbageFound = len(_GarbageFound)
print ''
print ''
print 'Total folders scanned: {}'.format(_FolderCount)
print 'Folders found inside the recyclebin: {}'.format(_intGarbageFound)
print 'cover.jpg found: {}'.format(_intCoverfound)
print 'cdart.png found: {}'.format(_intCDArtfound)
print 'folder.jpg found: {}'.format(_intFolderJPGfound)
print 'thumbs.jpg found: {}'.format(_intThumbsDBfound)
print ''
print ''

_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Deleting Recyclebin **********' % _timeNOW)
print ''
print 'I would delete: {}'.format(_GarbageFound[0])
#shutil.rmtree(_strCurrentFolder)
print ''
print ''

_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Checking cover.jpg sizes **********' % _timeNOW)
print ''
_ToBeDeletedLaterCOVER = []
_ToBeCheckedLaterCOVER = []
for i in _CoverFilesFound:
	_size = ImageResolution(i)
	if _size == -1:
		_ToBeCheckedLaterCOVER.append(i)
	elif _size is not None:
		if (_size[0] < 1000) or (_size[1] < 1000):
			print 'NOT OK: {}x{} --> {}'.format(_size[0],_size[1],i)
			_ToBeDeletedLaterCOVER.append(i)
		elif (_size[0] > 1000) or (_size[1] > 1000):
			print 'Need manual check: {}x{} --> {}'.format(_size[0],_size[1],i)
			_ToBeCheckedLaterCOVER.append(i)
		else:
			if _VerboseOutput: print 'OK: {}'.format(i)
	else:
		if _VerboseOutput: print 'ERROR: unable to detect image size for: {}'.format(i)
print ''
print ''


_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Checking cdart.png sizes **********' % _timeNOW)
print ''
_ToBeDeletedLaterCDART = []
_ToBeCheckedLaterCDART = []
for i in _CDArtFilesFound:
	_size = ImageResolution(i)
	if _size == -1:
		_ToBeCheckedLaterCDART.append(i)
	elif _size is not None:
		if (_size[0] < 1000) or (_size[1] < 1000):
			print 'NOT OK: {}x{} --> {}'.format(_size[0],_size[1],i)
			_ToBeDeletedLaterCDART.append(i)
		elif (_size[0] > 1000) or (_size[1] > 1000):
			print 'Need manual check: {}x{} --> {}'.format(_size[0],_size[1],i)
			_ToBeCheckedLaterCDART.append(i)
		else:
			if _VerboseOutput: print 'OK: {}'.format(i)
	else:
		if _VerboseOutput: print 'ERROR: unable to detect image size for: {}'.format(i)
print ''
print ''


_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Deleting invalid cover.jpg\'s **********' % _timeNOW)
print ''
if LogDeletedFiles: writeDeletedLog('Deleted invalid cover.jpg\'s\n')
_DeletedFilesCover = DeleteListOfFiles(_ToBeDeletedLaterCOVER)
_DeletedFilesCover = len(_DeletedFilesCover)
print ''
print ''


_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Deleting invalid cdart.png\'s **********' % _timeNOW)
print ''
if LogDeletedFiles: writeDeletedLog('Deleted invalid cdart.png\'s\n')
_DeletedFilesCDart = DeleteListOfFiles(_ToBeDeletedLaterCDART)
_DeletedFilesCDart = len(_DeletedFilesCDart)
print ''
print ''

_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Deleting folder.jpg\'s **********' % _timeNOW)
print ''
if LogDeletedFiles: writeDeletedLog('Deleted invalid folder.jpg\'s\n')
_DeletedFilesFolder = DeleteListOfFiles(_FolderJPGFound)
_DeletedFilesFolder = len(_DeletedFilesFolder)
print ''
print ''


_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Deleting thumbs.db\'s **********' % _timeNOW)
print ''
if LogDeletedFiles: writeDeletedLog('Deleted invalid thumbs.db\'s\n')
_DeletedFilesThumbs = DeleteListOfFiles(_ThumbsDBFound)
_DeletedFilesThumbs = len(_DeletedFilesThumbs)
print ''
print ''


_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: Listing exceptions **********' % _timeNOW)
print ''
if LogDeletedFiles: writeDeletedLog('Exceptions cover.jpg\n')
if LogDeletedFiles: writeDeletedLog("\n".join(_ToBeCheckedLaterCOVER))
print ("\n".join(_ToBeCheckedLaterCOVER))
if LogDeletedFiles: writeDeletedLog('\n\n')

print ''
if LogDeletedFiles: writeDeletedLog('Exceptions cdart.png\n')
if LogDeletedFiles: writeDeletedLog('\n')
if LogDeletedFiles: writeDeletedLog("\n".join(_ToBeCheckedLaterCDART))
print ("\n".join(_ToBeCheckedLaterCDART))
#if LogDeletedFiles: writeDeletedLog('\n\n')
print ''
print ''

_timeNOW = time.strftime("%Y-%m-%d %H:%M:%S")
_timeSTOP = time.strftime("%Y-%m-%d %H:%M:%S")
print ('********** %s: statistics **********' % _timeNOW)
print ''
_coverCount = len(_CoverFilesFound)
_cdartCount = len(_CDArtFilesFound)
_coverExcCount = len(_ToBeCheckedLaterCOVER)
_cdartExcCount = len(_ToBeCheckedLaterCDART)
print 'Scanned folders: {}'.format(_FolderCount)
print 'Found and checked: {} coverart\'s and deleted {} invalid file(s)'.format(_coverCount, _DeletedFilesCover)
print 'Found and checked: {} cdart\'s and deleted {} invalid file(s)'.format(_cdartCount, _DeletedFilesCDart)
print 'Exceptions found: cover.jpg: {}, cdart.png: {}'.format(_coverExcCount, _cdartExcCount)
print ''
print 'Deleted folder.jpg\'s: {}'.format(_DeletedFilesFolder)
print 'Deleted Thumbs.db\'s: {}'.format(_DeletedFilesThumbs)
print ''
print 'Scan started: {}'.format(_timeStartTime)
print 'Scan completed: {}'.format(_timeSTOP)
d1 = datetime.strptime(_timeStartTime, "%Y-%m-%d %H:%M:%S")
d2 = datetime.strptime(_timeSTOP, "%Y-%m-%d %H:%M:%S")
_timeDelta = abs((d2 - d1).seconds)
_timeDelta = time.strftime('%H:%M:%S', time.gmtime(_timeDelta))
print 'Scanning duration: {}'.format(_timeDelta)
print ''
print ''
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print '           Fanart maintenance script by Kamaradski V1.0'
print ' -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-'
print ''
print ''
# EOF
